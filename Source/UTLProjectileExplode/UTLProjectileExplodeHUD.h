// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once 

#include "CoreMinimal.h"
#include "GameFramework/HUD.h"
#include "UTLProjectileExplodeHUD.generated.h"

UCLASS()
class AUTLProjectileExplodeHUD : public AHUD
{
	GENERATED_BODY()

public:
	AUTLProjectileExplodeHUD();

	/** Primary draw call for the HUD */
	virtual void DrawHUD() override;

private:
	/** Crosshair asset pointer */
	class UTexture2D* CrosshairTex;

};

