// Copyright Epic Games, Inc. All Rights Reserved.

#include "UTLProjectileExplodeGameMode.h"
#include "UTLProjectileExplodeHUD.h"
#include "UTLProjectileExplodeCharacter.h"
#include "UObject/ConstructorHelpers.h"

AUTLProjectileExplodeGameMode::AUTLProjectileExplodeGameMode()
	: Super()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnClassFinder(TEXT("/Game/FirstPersonCPP/Blueprints/FirstPersonCharacter"));
	DefaultPawnClass = PlayerPawnClassFinder.Class;

	// use our custom HUD class
	HUDClass = AUTLProjectileExplodeHUD::StaticClass();
}
